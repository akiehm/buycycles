from django.urls import path

from .views import (
    api_bicycle,
    api_bicycles,
    api_manufacturers,
    api_manufacturer,
    api_bicycle_models,
    api_bicycle_model,
)

urlpatterns = [
    path(
        "bicycles/",
        api_bicycles,
        name="api_bicycles",
    ),
    path(
        "bicycles/<str:serial>/",
        api_bicycle,
        name="api_bicycle",
    ),
    path(
        "manufacturers/",
        api_manufacturers,
        name="api_manufacturers",
    ),
    path(
        "manufacturers/<int:pk>/",
        api_manufacturer,
        name="api_manufacturer",
    ),
    path(
        "models/",
        api_bicycle_models,
        name="api_bicycle_models",
    ),
    path(
        "models/<int:pk>/",
        api_bicycle_model,
        name="api_bicycle_model",
    ),
]
