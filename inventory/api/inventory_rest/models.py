from django.db import models
from django.urls import reverse


class Manufacturer(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_manufacturer", kwargs={"pk": self.id})


class BicycleModel(models.Model):
    name = models.CharField(max_length=100)
    picture_url = models.URLField()

    manufacturer = models.ForeignKey(
        Manufacturer,
        related_name="models",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_bicycle_model", kwargs={"pk": self.id})


class Bicycle(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    serial = models.CharField(max_length=20, unique=True)

    model = models.ForeignKey(
        BicycleModel,
        related_name="bicycles",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_bicycle", kwargs={"serial": self.serial})
