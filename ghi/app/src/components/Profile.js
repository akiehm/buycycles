import React from 'react';
import { useAuth0, withAuthenticationRequired } from '@auth0/auth0-react';


const Profile = () => {
    const { user, isAuthenticated } = useAuth0();

    return (
        isAuthenticated && (
            <div className="text-center">
                <br></br>
                <h2>Welcome, {user.name}</h2>
                <h3>MyCycles</h3>
                <table className='table table-sm table-dark table-striped table-bordered table-hover mt-5'>
                <thead>
                    <tr>
                    <th>Serial</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Model Image</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                </table>
            </div>
            
        )
    )
}

export default withAuthenticationRequired(Profile);