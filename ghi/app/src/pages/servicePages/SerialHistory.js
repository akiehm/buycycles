import React from 'react';

class SerialHistory extends React.Component{
    constructor(props){
        super(props);
        this.state={
            search: '',
            appointments:[]
        };
    this.handleChangeSearch = this.handleChangeSearch.bind(this);

    }
    async componentDidMount(){
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({appointments: data.appointments})
        }
    }
   

    handleChangeSearch(event){
        const value = event.target.value;
        this.setState({ search: value })
    }
    
    render() {
        return (
            <div>
                <input onChange={this.handleChangeSearch} type="text" className="form-control mt-5" placeholder="Search Serial Numbers"/>
                <table className='table table-sm table-dark table-striped table-bordered table-hover mt-5'>
                <thead>
                    <tr>
                    <th>Serial Number</th>
                    <th>Owner</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.appointments.map((appointment) => {
                        let isComplete=""
                        if(appointment.is_finished === true){
                        isComplete="table-success"
                        }
                        let isVip=""
                        if(appointment.is_vip===true){
                        isVip="table-dark"
                        }
                        let hideList = ""
                        if(appointment.serial !== this.state.search){
                            hideList="d-none"
                        }
                        
                    return(
                        <tr className={isComplete} key={appointment.id}>
                            <td className={isVip+" "+hideList}>{appointment.serial}</td>
                            <td className={hideList}>{appointment.owner}</td>
                            <td className={hideList}>{appointment.date}</td>
                            <td className={hideList}>{appointment.time}</td>
                            <td className={hideList}>{appointment.technician}</td>
                            <td className={hideList}>{appointment.reason}</td>
                        </tr>
                    );
                    })}
                    <p>*VIP customers are highlighted.</p>
                </tbody>
                </table>
            </div>

        );
    }
}
export default SerialHistory;