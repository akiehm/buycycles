import React from 'react';

class BicycleList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      bicycles: []
    };
  };

  async componentDidMount() {
    const bicyclesURL = "http://localhost:8100/api/bicycles/";
    const response = await fetch(bicyclesURL);
    if (response.ok) {
        const data = await response.json();
        this.setState({ bicycles: data.bikes})
    }
  }
  render (){
    return (
        <table className='table table-sm table-dark table-striped table-bordered table-hover mt-5'>
          <thead>
            <tr>
              <th>Serial</th>
              <th>Color</th>
              <th>Year</th>
              <th>Manufacturer</th>
              <th>Model</th>
              <th>Model Image</th>
            </tr>
          </thead>
          <tbody>
            {this.state.bicycles.map(bicycle => {
              return(
                  <tr key={bicycle.id}>
                    <td>{bicycle.serial}</td>
                    <td>{bicycle.color}</td>
                    <td>{bicycle.year}</td>
                    <td>{bicycle.model.manufacturer.name}</td>
                    <td>{bicycle.model.name}</td>
                    <td><img src={ bicycle.model.picture_url }/></td>
                  </tr>
              );
            })}
          </tbody>
        </table>
    )
  }
}
export default BicycleList;