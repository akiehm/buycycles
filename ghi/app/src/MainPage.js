import { Button, Card } from 'react-bootstrap';


function MainPage() {
  return (
    <>
      <div className="big-image shadow">
        <div className="overlay">
          <h1 className="display-5 fw-bold">BuyCycles</h1>
          <p className="lead mb-4">The premiere solution for bicycle dealership management!</p>
        </div>
      </div>

      <br></br>

      <div className="card shadow text-center bg-light border-secondary mb-3" style={{maxWidth: "100%"}}>
      <div className="row g-0">
        <div className="col-md-4">
          <img src="https://images.unsplash.com/photo-1571333249291-a6ec5e134a21?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80" 
            className="img-fluid rounded-start" alt="..." />
        </div>
        <div className="col-md-8">
          <div className="card-body">
            <h3 className="card-title">Bicycle Inventory</h3>
            <h5>Looking for that perfect new bike? Look no further!</h5>

            <p className="card-text">
                Visit our Inventory portal to see our awesome bikes!
                We restock weekly so be sure to check back! Be sure to check out the models we carry.

             </p>
             <br></br>
            <div className="d-grid gap-2 col-6 mx-auto">
              <Button variant="secondary" size="lg" href="/bicycles">See Our Inventory!</Button>{' '}
            </div>
          </div>
        </div>
      </div>
      </div>

      <div className="card shadow text-center bg-light border-secondary mb-3" style={{maxWidth: "100%"}}>
      <div className="row g-0">
        <div className="col-md-8">
          <div className="card-body">
            <h3 className="card-title">Bicycle Service Portal</h3>
            <h5>Need repairs or specialty services for your bike?</h5>
            <p className="card-text">
                You've come to the right place!
                Check out our Service portal to schedule a maintenance or repair appoitment with one of our world-class technicians.

                Let us know how we can help!
             </p>
             <br></br>
            <div className="d-grid gap-2 col-6 mx-auto">
              <Button variant="secondary" size="lg" href="/appointments/new">Schedule Service Appointment</Button>{' '}
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <img src="https://images.unsplash.com/photo-1592222166121-93437e78d8d0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1472&q=80" 
            className="img-fluid rounded-start" alt="..." />
        </div>
      </div>
      </div>

    </>
  );
}

export default MainPage;
