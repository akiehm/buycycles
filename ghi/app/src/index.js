import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { Auth0Provider } from '@auth0/auth0-react';
import { BrowserRouter } from 'react-router-dom';


const root = ReactDOM.createRoot(document.getElementById('root'));

const domain = process.env.REACT_APP_AUTH0_DOMAIN;
const clientId = process.env.REACT_APP_AUTH0_CLIENT_ID;


async function loadInventory() {
  let manufacturersData, modelsData, bicyclesData,technicianData, appointmentData, salesrecordsData;
  const manufacturersResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const modelsResponse = await fetch('http://localhost:8100/api/models/');
  const bicyclesResponse = await fetch('http://localhost:8100/api/bicycles/');
  const technicianResponse = await fetch('http://localhost:8080/api/technicians/');
  const appointmentResponse = await fetch('http://localhost:8080/api/appointments/');
  const salesrecordsResponse = await fetch('http://localhost:8090/api/salesrecords/');
  
  if (manufacturersResponse.ok) {
    manufacturersData = await manufacturersResponse.json();
  } else {
    console.error("manufacturersResponse", manufacturersResponse);
  }
  if (modelsResponse.ok) {
    modelsData = await modelsResponse.json();
  } else {
    console.error("modelsResponse", modelsResponse);
  }
  if (bicyclesResponse.ok) {
    bicyclesData = await bicyclesResponse.json();
  } else {
    console.error("bicyclesResponse", bicyclesResponse);
  }
  if (technicianResponse.ok) {
    technicianData = await technicianResponse.json();
  } else {
    console.error("techniciansResponse", technicianResponse);

  }
  if (appointmentResponse.ok) {
    appointmentData = await appointmentResponse.json();
  } else {
    console.error("appointmentResponse", appointmentResponse);
  }
  if (salesrecordsResponse.ok) {
    salesrecordsData = await salesrecordsResponse.json();
  } else {
    console.error("salesrecordsResponse", salesrecordsResponse);

  }

  root.render(
    <BrowserRouter>
      <Auth0Provider
        domain={domain}
        clientId={clientId}
        redirectUri={window.location.origin}>
        <App manufacturers={manufacturersData.manufacturers} 
          models={modelsData.models}
          salesrecords={salesrecordsData.sales}
          bicycles={bicyclesData.bikes}
          technicians={technicianData.technicians}
          appointments={appointmentData.appointments} />
      </Auth0Provider>
    </BrowserRouter>
  );
}

loadInventory();