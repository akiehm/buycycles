import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import BicycleVO

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            # Changed Inventory URL below to bicycles
            response = requests.get("http://inventory-api:8000/api/bicycles/")
            content = json.loads(response.content)
            for bike in content["bikes"]:
                BicycleVO.objects.update_or_create(
                    serial=bike["serial"],
                )
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
