# BuyCycles



## Design
BuyCycles is a full-stack web application created for bicycle inventory, sales records, and services. There are three microservices: Bicycle inventory, bicycle sales, and bicycle services. Data is polled from the inventory service into the other two.

The website was styled using Bootstrap. Images were sourced from Unsplash.
